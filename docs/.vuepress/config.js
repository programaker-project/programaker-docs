module.exports = {
    title: 'Programaker documentation',
    dest: 'public',  // Destination directory for the build step
    head: [  // Favicon
        ['link', { rel: 'icon', href: '/favicon.ico' }]
    ],
    themeConfig: {
        sidebar: [
            ['/tutorials/quickstart.md', 'Quickstart'],
            '/developers/build-a-bridge/',
            '/developers/bridge-communication-protocol.md',
            '/support/',
            '/legal/privacy-policy',
            ['/legal/terms-of-service', 'Terms and Conditions of Use'],
        ],
        logo: '/assets/img/logo.png'
    },
}
